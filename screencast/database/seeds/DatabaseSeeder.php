<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //factory(\App\Post::class, 20)->create();
        // $this->call(UsersTableSeeder::class);

        factory(\App\User::class)->create([
            'name' => 'Vitor',
            'email' => 'vitordangelo@gmail.com',
            'password' => bcrypt(123456),
            'remember_token' => str_random(10),
        ]);
    }
}
