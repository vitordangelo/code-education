<?php

use Illuminate\Database\Seeder;

class oauth_clients extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('oauth_clients')->insert([
            'id' => 1,
            'secret' => "secret",
            'name' => "client1",
            'created_at' =>'2016-07-02 21:03:17',
            'updated_at' =>'2016-07-02 21:03:17',
        ]);
    }
}
