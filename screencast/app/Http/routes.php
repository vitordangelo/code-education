<?php

Route::group(['before'=>'oauth'], function() {
    Route::resource('post', 'ApiController@index');
});

Route::post('oauth/access_token', function() {
    return Response::json(Authorizer::issueAccessToken());
});
